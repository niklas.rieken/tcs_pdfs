\documentclass[12pt, a4paper]{article}
\usepackage[top=1cm,bottom=2cm,left=2.5cm, right=2.5cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage[osf]{mathpazo}
\usepackage{eulervm}
\usepackage{array}
\usepackage{graphicx}
\usepackage{float}
\usepackage{commath}
\usepackage{tikz}
\usepackage{pgfplots}
\usepgfplotslibrary{patchplots}
\pgfplotsset{compat=newest}
\pgfplotsset{plot coordinates/math parser=false}
\usepackage{tikz-3dplot}
\usetikzlibrary{calc,intersections}
\usepackage{listings}
\usepackage{enumitem}
\usepackage{algorithm2e}
\usepackage{wrapfig}
%\usepackage{bbold}
\usepackage{bbm}
\usepackage[justification=centering]{caption}
\usetikzlibrary{arrows, automata, graphs, shapes, petri, decorations.pathmorphing}
\parindent = 0pt
\frenchspacing
\let\emptyset\varnothing
\let\epsilon\varepsilon
\let\rho\varrho
\let\theta\vartheta
\let\phi\varphi
\DeclareMathOperator{\spa}{span}
\DeclareMathOperator{\aff}{aff}
\DeclareMathOperator{\conv}{conv}
\DeclareMathOperator{\cone}{cone}
\DeclareMathOperator{\dom}{dom}
\DeclareMathOperator{\epi}{epi}
\DeclareMathOperator{\inter}{int}
\DeclareMathOperator{\tr}{tr}

% define environments
\theoremstyle{definition}
\newtheorem{definition}{Definition}
\newtheorem*{remark*}{Remark}

\theoremstyle{plain}
\newtheorem{lemma}[definition]{Lemma}
\newtheorem{theorem}[definition]{Theorem}

\renewcommand{\labelenumi}{(\roman{enumi})}
\renewcommand{\labelenumi}{(\roman{enumi})}

\author{Niklas Rieken}
\title{A Primer in Convex Optimization}

\begin{document}
\maketitle

We consider mathematical optimization problems
\begin{align*}
	\text{minimize }	& f_0(x)\\
	\text{subject to }	& f_i(x) \leq b_i & i \in \{1, \ldots, m\}
\end{align*}
with  $x = (x_1, \ldots, x_n)^T \in \mathbb{R}^n$, the \emph{optimization variable}, $f_0\colon \mathbb{R}^n \to \mathbb{R}$, the \emph{objective function}, $f_i\colon \mathbb{R}^n \to \mathbb{R}$ for $i \in \{1, \ldots, m\}$, the \emph{constraint functions}, and $b_i \in \mathbb{R}$, the \emph{limits} (or \emph{bounds}, \emph{right hand side}) of constraints. 

The set of all points that satisfy the constraints
$$
	F = \{x \in \mathbb{R}^n \mid f_i(x) \leq b_i, i \in \{1, \ldots, m\}\}
$$
is called \emph{feasible set}. A point $x^\ast \in F \subseteq \mathbb{R}^n$ with $f_0(x^\ast) \leq f_0(x)$ for all $x \in F$ is called an \emph{(optimal) solution}.

We distinguish (for now) three classes od optimization problems:
\begin{description}
	\item[Linear Programs (LP)] All $f_i$ for $i \in \{0, \ldots, m\}$ are linear functions, i.e. $f_i(x) = a_i^T x = \sum_{j=1}^n a_{ij} x_j$ and it holds that $f_i(\alpha a + \beta b) = \alpha f_i(a) + \beta f_i(b)$.
	\item[Nonlinear Programs (NLP)] All $f_i$ for $i \in \{0, \ldots, m\}$ can be arbitrary nonlinear.
	\item[Convex Optimization Problems] All $f_i$ for $i \in \{0, \ldots, m\}$ are convex functions, i.e. $f_i(\alpha a + \beta b) \leq \alpha f_i(a) + \beta f_i(b)$ for $\alpha + \beta = 1$ and $\alpha, \beta \geq 0$.
\end{description}
Note that every LP is a convex optimization problem and every convex optimization problem is an NLP. In particular, every LP is also an NLP.

Intuitively, $\alpha a + \beta b$ with $\alpha + \beta = 1$ and $\alpha, \beta \geq 0$ is a straight line segment between $a$ and $b$: $\alpha a + \beta b = (1-\beta)a + \beta b = a + \beta(b-a)$.
\begin{figure}[H]
	\centering
	\begin{tikzpicture}
		\path (0, 0) coordinate(a) -- (2, 1) coordinate(b);
		\draw[-] (a) -- (b) node[pos=-.1] {$a$} node[pos=1.1] {$b$};
		\draw[->,shorten >= .2cm] (.2, 0) -- +($(b) - (a)$) node[pos=.5, below right] {\scriptsize$(b-a)$};
	\end{tikzpicture}
\end{figure}
Hence, the condition for a function $f$ to be convex is that any straight line segment between two arbitrary points $(a, f(a)), (b, f(b))$ is above every point of the function between $a$ and $b$.
\begin{figure}[H]
	\centering
	%\begin{tikzpicture}
	%	\begin{axis}[axis y line=left, xmin=0, xmax=5, xlabel=$x$, ylabel=$f(x)$, ymin=-3.5, ymax=2, xtick={\empty}, ytick={\empty}, axis on top, axis x line=bottom, every axis plot post/.append style={mark=none,domain=0:5,samples=50,smooth}]
	%		\addplot{x^2 - 5*x + 4};
	%	\end{axis}
	%\end{tikzpicture}
	\begin{tikzpicture}
		\begin{axis}[width=5in,axis equal image,
		    axis lines=middle,
		    xmin=0,xmax=8,
			xlabel=$x$,ylabel=$f(x)$,
		    ymin=0,ymax=4,
		    xtick={\empty},ytick={\empty}, axis on top
		]
			\addplot[thick,domain=.25:7,blue,name path=A] {-x/3 + 2.75} coordinate[pos=0.4] (m) ;
			\draw[thick,blue,name path=B] (0.15,4) .. controls (1,1) and (4,0) .. (6,2) coordinate[pos=0.075] (a1) coordinate[pos=0.95] (a2);
			\path[name intersections={of=A and B, by={a,b}}];
			\draw[densely dashed] (0,0) -| node[pos=0.5, color=black, below] {$a$}(a);
			\draw[densely dashed] (0,0) -| node[pos=0.5, color=black, below] {$b$}(b);
		\end{axis}
	\end{tikzpicture}
\end{figure}

\section{Theory}
\subsection{Convex Sets}
In this primer, we assume vector spaces to be euclidian, i.e. $V = \mathbb{R}^n$. A \emph{linear combination} of vectors is $w = \alpha_1 v_1 + \ldots + \alpha_n v_n$ with $\alpha_i \in \mathbb{R}$ and $v_i \in \mathbb{R}^n$ for all $i \in \{1, \ldots, n\}$. For $S \subseteq \mathbb{R}^n$, 
$$
	\spa S \coloneqq \left\{\sum_{i=1}^n \alpha_i v_i : \alpha_i \in \mathbb{R}, v_i \in S\right\}
$$
denotes the \emph{linear hull} (or \emph{span}) of $S$.

An \emph{affine combination} of vectors is $w = \alpha_1 v_1 + \ldots + \alpha_n v_n$ with $\sum_{i=1}^n \alpha_i = 1$. Affine sets form a translated vector space:
\begin{align*}
	w = \sum_{i=1}^n \alpha_i v_i &= \alpha_1 v_1 + \sum_{i=2}^n \alpha_i v_i\\
	&= \left(1 - \sum_{i=2}^n \alpha_i\right) v_i + \sum_{i=2}^n \alpha_i v_i\\
	&= v_1 + \sum_{i=2}^n \alpha_i (v_i - v_1).
\end{align*}
For $S \subseteq \mathbb{R}^n$, 
$$
	\aff S \coloneqq \left\{\sum_{i=1}^n \alpha_i v_i : \alpha_i \mathbb{R}, v_i \in S \mid \sum_{i=1}^n \alpha_i = 1\right\}
$$
denotes the \emph{affine hull} of $S$.

A \emph{convex combination} of vectors is $w = \alpha_1 v_1 + \ldots + \alpha_n v_n$ with $\alpha_i \geq 0$ for all $i \in \{1, \ldots, n\}$ and $\sum_{i=1}^n \alpha_i = 1$. For $S \subseteq \mathbb{R}^n$, 
$$
	\conv S \coloneqq \left\{\sum_{i=1}^n \alpha_i v_i : \alpha_i \in \mathbb{R}_+, v_i \in S \mid \sum_{i=1}^n \alpha_i =1\right\}
$$
denotes the \emph{convex hull} of $S$.
\begin{figure}
	%TODO simplices
\end{figure}

In a convex set, the straight line segment between any two points of the set is a subset of the set.
\begin{figure}
	%TODO convex and non-convex set
\end{figure}

A \emph{conic combination} of vectors is $w = \alpha_1 v_1 + \ldots + \alpha_n v_n$ with $\alpha_i \geq 0$. For $S \subseteq \mathbb{R}^n$,
$$
	\cone S \coloneqq \left\{\sum_{i=1}^n \alpha_i v_i : \alpha_i \in \mathbb{R}_+, v_i \in S\right\}
$$
denotes the \emph{conic hull} of $S$.
\begin{figure}
	%TODO conic hull
\end{figure}

Note that vector spaces, affine sets and convex cones are all convex sets. %TODO proof?

In the following, we list some important examples for convex sets:
\begin{description}
	\item[hyperplane] $\{x \mid a^T x = b\} = \{x \mid a^T(x-x_0) = 0\}$ with \emph{normal vector} $a \neq 0$. Hyperplanes are affine.
	\item[halfspace] $\{x \mid a^T x \leq b\} = \{x \mid a^T (x-x_0\}$ with $a \neq 0$.
	\item[Euclidian ball] $B(x_c, r) = \{x \mid | \norm{x-x_c}_2 \leq r\} = \{x_c + ru : \norm{u}_2 \leq 1\}$, where $x_c$ is the center and $r$ the radius. 
	\item[ellipsoid] $\mathcal{E} = \{x \mid (x-x_c)^T P^{-1} (x-x_c) \leq 1\}$ with $P \in S_{++}^n$. The length of semi-axes are $\sqrt{\lambda_i}$ where $\lambda_i$ are the eigenvalues of $P$. Alternatively, $\mathcal{E}$ can be represented as $\{x_c + Au : \norm{u}_2 \leq 1\}$ for a regular matrix $A$ ($P = AA^T$).
	\item[norm ball] $\{x \mid \norm{x-x_c} \leq r$, where $x_c$ is the center, $r$ the radius, and $\norm{\cdot}$ an arbitrary norm.
	\item[norm cone] $\{(x, t) \mid \norm{x} \leq t\}$, i.e. stack norm balls with increasing radius.
	\item[polyhedron] $\{x \mid Ax \preceq b, Cx = d\}$ with $A \in \mathbb{R}^{m \times n}, C \in \mathbb{R}^{p \times n}$
	\item[positive semidefinite cone] $S_+^n = \{X \in S^n \mid X \succeq 0\}$ with $X \succeq 0$ if $z^T X z \geq 0$ for all $z \in \mathbb{R}^n$.
\end{description}
\begin{figure}
	%TODO all of the above
\end{figure}

If we want to prove for a set $C$ that it is convex, we often can just apply the definition, i.e. show for arbitrary $x, y \in C$ and $\theta \in [0, 1]$, that $(1-\theta)x + \theta y \in C$. In some cases it might be easier to show that $C$ is obtained from simple convex sets (e.g. hyperplanes, halfspaces, norm balls) by operations that preserve convexity.

\begin{lemma}
	Let $\{S_i\}_{i \in I}$ be an (maybe infinite) set of convex sets. Then $\bigcap_{i} S_i$ is also convex.
	%TODO proof
\end{lemma}
\begin{lemma}
	Let $f\colon \mathbb{R}^n \to \mathbb{R}^m$ be affine, i.e. $f(x) = Ax + b$. 
	\begin{enumerate}
		\item If $S \subseteq \mathbb{R}^n$ is convex, then $f[S] = \{f(x) : x \in S\}$ is convex.
		\item If $C \subseteq \mathbb{R}^m$ is convex, then $f^{-1}[C] = \{x \in \mathbb{R}^n \mid f(x) \in C\}$ is convex.
	\end{enumerate}
	%TODO proof
\end{lemma}
\begin{lemma}
	Let $P\colon \mathbb{R}^{n+1} \to \mathbb{R}^n, (x, t) \mapsto \frac{x}{t}$ with $\dom P = \{(x, t) \mid t > 0\}$ denote the \emph{perspective function}. Let $S \subseteq \mathbb{R}^{n+1}$ be convex. Then $P[S]$ is also convex.
	%TODO proof
\end{lemma}
\begin{lemma}
	Let $f\colon \mathbb{R}^n \to \mathbb{R}^m, x \mapsto \frac{Ax +b}{c^Tx +d}$ with $\dom f = \{x \mid c^Tx + d > 0\}$.
	\begin{enumerate}
		\item If $S \subseteq \mathbb{R}^n$ is convex, then $f[S]$ convex.
		\item If $C \subseteq \mathbb{R}^m$ is convex, then $f^{-1}[C]$ is convex.
	\end{enumerate}
	%TODO proof
\end{lemma}

We already informally used the symbols $\preceq, \succeq$ for component-wise inequalities for vectors and positive semidefinite matrices, respectively. The mathematical justification for these notions can be obtained by definitions via convex cones. We call a convex cone \emph{proper} if it is closed (i.e. contains its boundary), solid (i.e. has non-empty interior), and pointed (i.e., contains no line).
\begin{figure}
	%TODO proper cone and not pointed cone.
\end{figure}

Let $K$ be a proper cone. The \emph{generalized inequality} $\preceq_K, \prec_K$ id defined by
\begin{enumerate}
	\item $x \preceq_K y$ if $y - x \in K$,
	\item $x \prec_K y$ if $y - x \in \inter K$.
\end{enumerate}
\begin{figure}
	%TODO K = IR² cone
\end{figure}

In general $\preceq$ is not a linear ordering; however, it is a transitive relation and it is monotone with respect to $+$.

We call $x \in S$ the \emph{minimum element} of $S$ w.r.t. $\preceq_K$ if $x \preceq_K y$ for all $y \in S$. We call it a \emph{minimal element} of $S$ w.r.t. $\preceq_K$ if $y \preceq_K x$ for $y \in S$ implies $x = y$.

\begin{theorem}[Separating Hyperplane Theorem]
	If $C$ and $D$ are non-empty disjoint convex sets there exists $a \neq 0, b$, such that $a^T x \leq b$ for $x \in C$ and $a^T x \geq b$ for $x \in D$.
	%TODO proof
\end{theorem}
\begin{figure}
	%TODO separating hyperplane
\end{figure}

A \emph{supporting hyperplane} to a set $C$ at boundary point $x_0$ is $\{x \mid a^Tx = a^T x_0\}$ with $a \neq 0$ and $a^Tx \leq a^Tx_0$ for all $x \in C$.
\begin{figure}
	%TODO supporting hyperplane
\end{figure}
\begin{theorem}[Supporting Hyperplane Theorem]
	If $C$ is a convex set, then there exists a supporting hyperplane at every boundary point of $C$.
	%TODO proof
\end{theorem}

The \emph{dual cone} of a cone $K$ is denoted by
$$
	K^\ast \coloneqq \{y \mid y^Tx \geq 0 \text{ for all } x \in K\}.
$$
Geometrically, $K^\ast$ is the intersection of halfspaces through origin with normal $-x$ for all $x \in K$.
\begin{figure}
	%TODO dual cone
\end{figure}
Note that $K^\ast$ is a cone and always convex, even if $K$ is not. Dual cones of proper cones are also proper.

For a set $S$ (not necessarily convex), $x$ is minimum element of $S$ iff for all $\lambda \succ_{K^\ast} 0$, it holds that $x = \arg\min_z \lambda^T z$ is unique minimizer. If $x \in S$ is minimum of $\lambda^T z$ for some $\lambda \succ_{K^\ast} 0$, then $x$ is minimal. Moreover, if $x$ is minimal element of a convex set $S$ then there exists non-zero $\lambda \succeq_{K^\ast} 0$ with $x = \arg\min_{z \in S} \lambda^T z$.
\begin{figure}
	%TODO for the weird stuff above
\end{figure}

We may call a point \emph{Pareto optimal} if it is minimal w.r.t. $K = \mathbb{R}_+^n$
\begin{figure}
	%TODO Pareto frontier
\end{figure}

\subsection{Convex Functions}
A function $f\colon \mathbb{R}^n \to \mathbb{R}$ is convex if 
\begin{enumerate}
	\item $\dom f$ is a convex set and
	\item $f((1-\theta)x + \theta y) \leq (1-\theta)f(x) + \theta f(y)$ for all $x, y \in \dom f$ and $\theta \in [0, 1]$.
\end{enumerate}
$f$ is called \emph{concave} if $-f$ is convex. We say $f$ is called \emph{strictly convex} if the second condition is strengthened to
\begin{enumerate}
	\item[(ii*)] $f((1-\theta)x + \theta y) < (1-\theta)f(x) - \theta f(y)$ for all $x, y \in \dom f$ and $\theta \in \left]0, 1\right[$.
\end{enumerate}
Conversely, $f$ is called \emph{strictly concave} if $-f$ ist strictly convex.

In the grand scheme of things, a lot of useful functions, that are common for practical applications happen to be convex or cancave. For functions on $\mathbb{R}$, we have for example:
\begin{description}
	\item[affine functions] $ax + b$ with $a, b \in \mathbb{R}$ are convex and concave,
	\item[exponential functions] $e^{ax}$ with $a \in \mathbb{R}$ are convex,
	\item[powers] $x^\alpha$ on $\mathbb{R}_{++}$ are convex if $\alpha \geq 1$ or $\alpha \leq 0$ and concave if $\alpha \in [0, 1]$,
	\item[powers of absolute values] $\abs{x}^p$ on $\mathbb{R}$ with $p \geq 1$ are convex,
	\item[logarithm] $\log x$ on $\mathbb{R}_{++}$ is concave,
	\item[negative entropy] $x \log x$ on $\mathbb{R}_{++}$ is convex.
\end{description}
Moreover, we have example on $\mathbb{R}^n$ and $\mathbb{R}^{n \times m}$:
\begin{description}
	\item[affine functions] $a^Tx + b$ or $\tr(A^TX) + b = \sum_i \sum_j a_{ij} x_{ij} + b$ are convex and concave,
	\item[norms] $\norm{\cdot}$ are always convex.
\end{description}

The following lemma justifies that we can restrict ourselves to lines in $\mathbb{R}^n$ when we want to proof convexity of a function on $\mathbb{R}^n$.
\begin{lemma}
	A function $f\colon \mathbb{R}^n \to \mathbb{R}$ is convex iff $g\colon \mathbb{R} \to \mathbb{R}$ given by $g(t) = f(x+tv)$ with $\dom g = \{t \mid x+tv \in \dom f\}$ is convex in $t$ for any $x \in \dom f, v \in \mathbb{R}^n$.\footnote{Alternatively: $g(t) = f((1-t)x + ty)$ with $x, y \in \dom f$ and $t \in [0, 1]$.}
	%TODO proof
\end{lemma}

For a function $f\colon \mathbb{R}^n \to \mathbb{R}$, we denote the \emph{extended-value extension} by 
$$
	\tilde{f}(x) \coloneq \begin{cases}
		f(x), & x \in \dom f\\
		\infty, & x \notin \dom f.
	\end{cases}
$$

A function $f\colon \mathbb{R}^n \to \mathbb{R}$ is \emph{differentiable} if $\dom f$ is open and the \emph{gradient}
$$
	\nabla f(x) = \left(
		\frac{\partial f(x)}{\partial x_1}, \ldots, \frac{\partial f(x)}{\partial x_n}
	\right)^T
$$
exists at each $x \in \dom f$.

\begin{theorem}[First-Order Condition]
	A differentiable function $f\colon \mathbb{R}^n\to \mathbb{R}$ with convex domain is convex iff for all $x, y \in \dom f$,
	$$
		f(y) \geq f(x) + \nabla f(x)^T (y-x).
	$$
	%TODO proof
\end{theorem}
Thus, the first-order Taylo approximation of $f$ in any point is a global underestimator.
\begin{figure}
	%TODO Taylor
\end{figure}
Moreover, from this theorem it follows that if $\nabla f(x) = 0$ then $f(y) \geq f(x)$, i.e. $x$ minimizes $f$.

A function $f\colon \mathbb{R}^n \to \mathbb{R}$ is \emph{twice differentiable} if $\dom f$ is open and the \emph{Hessian}
$$
	\nabla^2 f(x) = \left[
		\frac{\partial^2 f(x)}{\partial x_i \partial x_j}
	\right]_{ij} \in S^n
$$
exists at each $x \in \dom f$.

\begin{theorem}[Second-order condition]
	A twice differentiable function $f\colon \mathbb{R}^n \to \mathbb{R}$ with convex domain is convex iff for all $x \in \dom f$,
	$$
		\nabla^2 f(x) \succeq 0.
	$$
	%TODO proof
\end{theorem}
Intuitively this means, that $f$ is convex if it has positive curvature in any point (''bends upwards``).

The \emph{$\alpha$-sublevel set} of $f\colon \mathbb{R}^n \to \mathbb{R}$ is given by
$$
	C_\alpha \coloneqq \{x \in \dom f \mid f(x) \leq \alpha\}.
$$
\begin{figure}
	%TODO sublevel set
\end{figure}
\begin{lemma}
	Sublevel sets of convex functions are convex.
	%TODO proof
\end{lemma}

The \emph{epigraph} of a function $f\colon \mathbb{R}^n \to \mathbb{R}$ is denoted by
$$
	\epi f \coloneqq \{(x, t) \in \mathbb{R}^{n+1} \mid x \in \dom f, t \geq f(x)\}.
$$
\begin{figure}
	%TODO epigraph
\end{figure}
\begin{lemma}
	A function $f$ is convex iff $\epi f$ is a convex set.
\end{lemma}

This establishes a connection between convex  sets and convex functions. For instance, the first-order condition for a function $f$ in $x$ corresponds to the supporting hyperplane theorem to $\epi f$ in $(x, f(x))$.
\begin{figure}
	%TODO foc vs epigraph w/ supporting hyperplane
\end{figure}

We now can verify convexity of a function with different means:
\begin{itemize}
	\item verify definition (restriction to lines suffice),
	\item for twice differentiable functions, show $\nabla^2 f(x) \succeq 0$,
	\item show that $f$ is obtained from simple convex functions that preserve convexity.
\end{itemize}

\begin{lemma}
	Let $f_1, f_2$ be convex functions and $\alpha \geq 0$. Then $\alpha(f_1 + f_2)$ is convex.
	%TODO proof
\end{lemma}
\begin{lemma}
	Let $f$ be a convex function and $g(x) = Ax +b$ an affine function. Then $f \circ g = f(Ax+b)$ is convex.
	%TODO proof
\end{lemma}
\begin{lemma}
	Let $(f_i)_{i=1}^m$ be a family of convex functions. Then $f(x) \coloneqq \max_{i=1,\ldots,m} f_i(x)$ is convex.
	%TODO proof
\end{lemma}
\begin{lemma}
	Let $f(x, y)$ be convex in $x$ for all $y \in A$. Then $g(x) = \sup_{y} f(x, y)$ is convex.
	%TODO proof
\end{lemma}
\begin{lemma}
	Let $f(x) = h \circ g(x) = h(g(x))$ with $g\colon \mathbb{R}^n \to \mathbb{R}$ and $h\colon \mathbb{R} \to \mathbb{R}$. $f$ is convex if $g$ is convex, $h$ is convex and $\tilde{h}' \geq 0$, or $g$ is concave, $h$ is convex and $\tilde{h}' \leq 0$.
	%TODO proof
\end{lemma}
\begin{lemma}
	If $f(x, y)$ is convex in $(x, y)$ and $C$ a convex set, then $g(x) = \inf_{y \in C} f(x, y)$ is convex.
	%TODO proof
\end{lemma}
\begin{lemma}
	Let $g\colon \mathbb{R}^{n+1} \to \mathbb{R}$ be the \emph{perspective} of $f\colon \mathbb{R}^n \to \mathbb{R}$ defined by $g(x, t) = t f(\frac{x}{t})$. $g$ is convex if $f$ is convex.
	%TODO proof
\end{lemma}

The \emph{conjugate} of a function $f(x)$ is defined by
$$
	f^\ast(y) = \sup_x (y^Tx - f(x)).
$$
This is the maximal signed distance to $f(x)$ for each linear function $y^T(\cdot)$. Note that $f^\ast$ is convex, even if $f$ is not. For convex functions it holds that $f^{\ast\ast} = f$.
\begin{figure}
	%TODO if possible conjugate function
\end{figure}

A function $f\colon \mathbb{R}^n \to \mathbb{R}$ is \emph{quasiconvex} if 
\begin{enumerate}
	\item $\dom f$ is convex,
	\item $C_\alpha$ is convex for all $\alpha \in \mathbb{R}$.
\end{enumerate}
$f$ is called \emph{quasiconcave} if $-f$ is quasiconvex and $f$ is called \emph{quasilinear} if $f$ is quasiconvex and quasiconcave. Note that every convex function is alsoe quasiconvex.

For a quasiconvex function $f$ and $\theta \in [0, 1]$ it holds that
$$
	f((1-\theta)x + \theta y) \leq \max \{f(x), f(y)\}.
$$

\begin{lemma}
	Let $f\colon \mathbb{R}^n \to \mathbb{R}$ be differentiable and $\dom f$ convex. $f$ is quasiconvex iff $f(y) \leq f(x)$ implies that $\nabla f(x)^T (y-x) \leq 0$.
	%TODO proof
\end{lemma}

Let $f\colon \mathbb{R}^n \to \mathbb{R}^m$ be a function and $K$ be a proper cone. $f$ is \emph{$K$-convex} if 
\begin{enumerate}
	\item $\dom f$ is convex,
	\item $f((1-\theta)x + \theta y) \preceq_K (1-\theta)f(x) + \theta f(y)$ for all $x, y \in \dom f$ and $\theta \in [0, 1]$. 
\end{enumerate}
Without further discussion, we state that many results for convex functions extend to $K$-convexity.

\subsection{Convex Optimization}
We consider general optimization problems ins \emph{standard form}:
\begin{align*}
	\text{minimize }	& f_0(x)\\
	\text{subject to }	& f_i(x) \leq 0	& i \in \{1, \ldots, m\}\\
						& h_i(x) = 0	& i \in \{1, \ldots, p\}.
\end{align*}
A feasible point $x$ is \emph{(globally) optimal} if $f_0(x) = \inf \{f_0(y) : y \text{ feasible}\}$. It is \emph{locally optimal} if there exists $R > 0$ such that $x$ is optimal in feasible ball $\norm{z-x} \leq R$. The \emph{feasibility problem} can be stated in standard form:
\begin{align*}
	\text{minimize }	& 0\\
	\text{subject to }	& f_i(x) \leq 0	& i \in \{1, \ldots, m\}\\
						& h_i(x) = 0	& i \in \{1, \ldots, p\}.
\end{align*}
An optimization problem is called \emph{convex} if $f_i$ is convex for all $i \in \{0, \ldots, m\}$ and $h_i$ is affine for all $i \in \{0, \ldots, p\}$.
\begin{theorem}
	Any locally optimal point of a convex optimization problem is globally optimal.
	%TODO proof
\end{theorem}

A point $x$ is optimal iff
\begin{enumerate}
	\item $x$ is feasible and
	\item $\nabla f_0(x)^T (y-x) \geq 0$ for all feasible $y$.
\end{enumerate}
\begin{figure}
	%TODO iso contours and gradient directions
\end{figure}

In order to simplify convex problems, we can use some common transformations:
\begin{itemize}
	\item Eliminating equality constraints ($Ax = b \iff x = Fz + x_0$ then minimize over $z$).
	\item Introducing equality constraints.
	\item Introducing slack variables for linear inequalities $a_i^T x + s_i = b_i, s_i \geq 0$.
	\item Epigraph form: minimize $t$ subject to $f_0(x) - t, (f_i(x) \leq 0)_{i=1}^m, Ax = b$.
\end{itemize}

A \emph{quasiconvex optimization problem} in standard form
\begin{align*}
	\text{minimize }	& f_0(x)\\
	\text{subject to }	& f_i(x) \leq 0	& i \in \{1, \ldots, m\}\\
						& Ax = b
\end{align*}
with $f_0$ quasiconvex, and $f_i$ convex for all $i \in \{1, \ldots, m\}$.

\begin{theorem}
	If $f_0$ is quasiconvex, then there exists a family of functions $(\phi_t)_t$ such that
	\begin{enumerate}
		\item $\phi_t(x)$ is convex in $x$ (for fixed $t$) and
		\item $t$-sublevel sets of $f_0$ is $0$-sublevel set of $\phi_t$, i.e. $f_0(x) \leq t$ iff $\phi_t(x) \leq 0$.
	\end{enumerate}
	%TODO proof
\end{theorem}

We can solve quasiconvex (and hence, convex) problems via bisection on $t$ in a feasibility problem:
\begin{align*}
	\text{minimize }	& 0\\
	\text{subject to }	& f_i(x) \leq 0	& i \in \{1, \ldots, m\}\\
						& Ax = b\\
						& \phi_t(x) \leq 0
\end{align*}
If this problem is feasible for a fixed $t$, then $t \geq p^\ast$ (optimum), if infeasible, then $t < p^\ast$.

We now approach the main theorems for convex optimization. That is, that in many cases optimality can be proved for a certain solution via duality.

Given a standard form problem, not necessarily convex
\begin{align*}
	\text{minimize }	& f_0(x)\\
	\text{subject to }	& f_i(x) \leq 0	& i \in \{1, \ldots, m\}\\
						& h_i(x) = 0	& i \in \{1, \ldots, p\}
\end{align*}
with $x \in D \subseteq \mathbb{R}^n$ and optimal value $p^\ast$, we define the \emph{Lagrangian}
$$
	L\colon \mathbb{R}^n \times \mathbb{R}^m \times \mathbb{R}^p \to \mathbb{R}
$$
with $\dom L = D \times \mathbb{R}^m \times \mathbb{R}^p$ by
$$
	L(x, \lambda, \nu) = f_0(x) + \sum_{i=1}^m \lambda_i f_i(x) + \sum_{i=1}^p \nu_i h_i(x)
$$
and the \emph{Lagrange dual function} $g\colon \mathbb{R}^m \times \mathbb{R}^p \to \mathbb{R}$ by
$$
	g(\lambda, \nu) = \inf_{x \in D} \underbrace{L(x, \lambda, \nu)}_{\text{affine on } \lambda, \nu}.
$$
Note that $g$ is concave. We call $\lambda, \nu$ \emph{Lagrange dual variables} (or \emph{Lagrange multipliers}).

\begin{theorem}[Lower Bound Property]
	If $\lambda \geq 0$ then $g(\lambda, \nu) \leq p^\ast$.
	%TODO proof
\end{theorem}

The following equation shows how the dual and the conjugate relate for minimize $f_0(x)$ subject to $Ax \preceq b, Cx = d$.
\begin{align*}
	g(\lambda, \nu) &= \inf_{x \in D} \left(f_0(x) + (A^T\lambda + c^T\nu)^Tx - b^T\lambda - d^T\nu\right)\\
	&= -f_0^\ast (-A^T\lambda - C^T\nu) - b^T\lambda - d^T\nu.
\end{align*}

The \emph{Lagrange dual problem} is given by
\begin{align*}
	\text{maximize }	& g(\lambda, \nu)\\
	\text{subject to }	& \lambda \succeq 0.
\end{align*}
Solving the Lagrange dual problem yields to the best lower bound of $p^\ast$, which we denote $d^\ast$. Note that the dual problem is always convex even if the primal problem is not. The Lagrange dual variables $\lambda, \nu$ are dual feasible if $\lambda \succeq 0$ and $\lambda, \nu \in \dom g$.

\begin{theorem}[Weak Duality]
	$d^\ast \leq p^\ast$.
	%TODO proof
\end{theorem}
Weak duality always holds for convex and non-convex problems and can be used to find non-trivial lower bounds for difficult problems. We call the difference between primal and dual solution $p^\ast - d^\ast$ \emph{(optimal) duality gap}.

The following theorem does actually not hold in general but it does usually hold for convex problems. 
\begin{theorem}[Strong Duality]
	$d^\ast = p^\ast$.
\end{theorem}
To make the ''usually`` in the previous statement more precise, consider \emph{Slater's constraint qualification}: Strong duality holds for convex problems in standard form if it is strictly feasible, i.e. there exists $x \in \inter D$ such that $f_i(x) < 0$ for all $i \in \{1, \ldots, m\}$ and $Ax = b$.

Another strong connection between primal and dual problem is due to the following theorem.
\begin{theorem}[Complementary Slackness]
	Assume strong duality holds and $x^\ast, \lambda^\ast, \nu^\ast$ are optimal for the primal, respectively, dual problem. Then $\lambda^\ast_i f_i(x^\ast) = 0$ for all $i \in \{1, \ldots, m\}$.
	\begin{proof}
		\begin{align*}
			f_0(x^\ast) &= g(\lambda^\ast, \nu^\ast)\\
			&= \inf_{x} \left(f_0(x) + \sum_{i=1}^m \lambda_i^\ast f_i(x) + \sum_{i=1}^p \nu_i^\ast h_i(x)\right)\\
			&\leq f_0(x^\ast) + \underbrace{\sum_{i=1}^m \lambda_i^\ast f_i(x^\ast)}_{\leq 0} + \underbrace{\sum_{i=1}^p \nu^\ast h_i(x^\ast)}_{= 0}\\
			&\leq f_0(x^\ast).\qedhere
		\end{align*}
	\end{proof}
\end{theorem}

Strong duality and the lack of a gradient descent direction that is feasible in optimal points yields to the next theorem, which generalizes the first-order optimality condition (not to be confused with the first-order convexity conition from the previous section) $f'(x) = 0$.
\begin{theorem}[Karush, Kuhn, Tucker]
	Assume strong duality holds and $x, \lambda, \nu$ are optimal, then
	\begin{enumerate}
		\item primal feasibility, i.e. $f_i(x) \leq 0$ for $i \in \{1, \ldots, m\}$, $h_i(x) = 0$ for $i \in \{1, \ldots, p\}$ holds,
		\item dual feasibility, i.e. $\lambda_i \geq 0$ for $i \in \{1, \ldots, m\}$ holds,
		\item complementary slackness, i.e. $\lambda_i f_i(x) = 0$ for $i \in \{1, \ldots, m\}$ holds, and
		\item gradient descent of Lagrangian w.r.t. $x$ vanishes, i.e.
			$$
				\nabla f_0(x) + \sum_{i=1}^m \lambda_i \nabla f_i(x) + \sum_{i=1}^p \nu_i \nabla h_i(x) = 0.
			$$
	\end{enumerate}
	%TODO proof
\end{theorem}

\begin{figure}
	%TODO again isolines and gradients
\end{figure}

We can even solve the primal problem via the dual: Assume we have optimal duals $(\lambda^\ast, \nu^\ast)$. Then it suffices to minimize $f_0(x) + \sum_{i=1}^m \lambda_i^\ast f_i(x) + \sum_{i=1}^p \nu_i^\ast h_i(x)$.

\begin{lemma}
	Let $x$ be primal feasible and $(\lambda, \nu)$ dual feasible. Then 
	$$
		f_0(x) - p^\ast \leq f_0(x) - g(\lambda, \nu).
	$$
	%TODO proof
\end{lemma}
Thus, $p^\ast \in [g(\lambda, \nu), f_0(x)]$ and $d^\ast \in [g(\lambda, \nu), f_0(x)]$, i.e. a duality gap of $0$ is a certificate for optimality.


\section{Algorithms}
For unconstrained problems, we can simply use the optimality condition $\nabla f_0(x^\ast) = 0$ as a starting point. We shortly describe a few algorithms that compute a solution numerically. Exact solving is in general not possible.
The main idea of our algorithms is very generic and can be described in pseudocode with a few lines.
\begin{algorithm}
	\While{stopping criterion not met}{
		Determine descent direction $\Delta x$.\\
		Line search: choose step size $t$.\\
		$x \coloneqq x + t \Delta x$.
	}
\end{algorithm}

This generic approach works for convex problems: From $\nabla f(x^{(k)}) (y-x^{(k)}) \geq 0$, we can derive $f(y) \geq f(x^{(k)})$. Hence, the search direction must satisfy $\nabla f(x^{(k)})^T \Delta x^{(k)} < 0$.

For the line search there exists two general ideas.
\begin{description}
	\item[exact line search] $t \coloneqq \arg\min_{t>0} f(x+t\Delta x)$ via binary search
	\item[backtracking line search] with parameters $\alpha \in \left]0, \frac{1}{2}\right[, \beta \in \left]0, 1\right[$. Backtrack until $t < t_0$.
		\vspace{-2em}
		\begin{algorithm}
			$t \coloneqq 1$.\\
			\While{$f(x+t\Delta x) \geq f(x) + \alpha t \nabla f(x)^T \Delta x$}{
				$t \coloneqq \beta t$.
			}
		\end{algorithm}
		\vspace{-2em}
\end{description}
\begin{figure}
	%TODO line searches
\end{figure}

To determine the descent direction, the obvious start would be the gradient descent method, that is choose $\Delta x = -\nabla f(x)$.

% eof
\end{document}
