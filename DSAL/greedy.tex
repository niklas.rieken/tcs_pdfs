\documentclass[12pt]{article}
\usepackage[left=2.5cm, right=2.5cm, top=3cm, bottom=3cm]{geometry}
\linespread{1.1}
\frenchspacing

\usepackage{setspace}
%\usepackage{footnote}
%\makesavenoteenv{tabular}
%\makesavenoteenv{table}
\usepackage{tablefootnote}
\usepackage{natbib}
\usepackage[utf8]{inputenc}
\usepackage[german]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{stmaryrd}
\usepackage{amsthm}
\usepackage{thmtools}
\usepackage{tikz}
\usetikzlibrary{calc}
\usepackage{graphicx}
\usepackage{mathrsfs}
\usepackage{mathdots}
\usepackage{listings}
\usepackage[linesnumbered, ruled, vlined]{algorithm2e}
\usepackage[inline]{enumitem}
\usepackage{wrapfig}
\usepackage{float}
\usepackage{array}
\usepackage[justification=centering]{caption}
\usepackage{subcaption}
\usepackage{epigraph}
\usepackage{xspace}
\usetikzlibrary{arrows, automata, graphs, shapes, petri, decorations.pathmorphing}
\usepackage{hyperref}
\usepackage{unicode-math}
\unimathsetup{math-style=ISO, partial=upright, nabla=upright}
\defaultfontfeatures{Scale=MatchLowercase}
\setmainfont[Numbers={OldStyle,Proportional}]{TeX Gyre Pagella}
\usepackage[OT1, euler-digits, euler-hat-accent]{eulervm}
\setmathfont{euler}
\setmathfont[range={up/num, bfup/num, it, bfit, scr, bfscr,
                    sfup, sfit, bfsfup, bfsfit, tt} 
            ]{Asana Math}
\setmathfont[range=bfcal, Scale=MatchUppercase, Alternate]{Asana Math}
\setmathfont[range=bb, Scale=MatchUppercase]{Asana Math}
\setmathfont[range=\setminus]{XITS Math}
\newtheorem{theorem}{Satz}
\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\def\I{\mathcal{I}}
\def\F{\mathcal{F}}
\def\coloneqq{:=}
\let\emptyset\varnothing

\newcommand{\cmd}[1]{\colorbox{red!20}{\textcolor{red!90}{\texttt{#1}}}}
\newcommand{\code}[1]{\texttt{#1}}
\usepackage{pgfplots}
\pgfplotsset{compat=1.13}
\pgfmathdeclarefunction{gauss}{2}{%
  \pgfmathparse{1/(#2*sqrt(2*pi))*exp(-((x-#1)^2)/(2*#2^2))}%
}

\lstset{showstringspaces=false, columns=flexible, framesep=10pt, language=bash, breaklines=true, breakatwhitespace=false, backgroundcolor=\color{black}, basicstyle=\small\ttfamily\color{white}, rulecolor=\color{gray}}

\title{Matroide und Greedy-Algorithmen}
\author{Niklas Rieken}


\begin{document}
\maketitle

Wir wiederholen zunächst die Definition von Matroiden
\begin{definition}
	Ein \emph{$\I$-Matroid} (kurz: \emph{Matroid}) $M = (E, \I)$ ist ein Paar aus endlicher Grundmenge $E$ und einer Familie $\I \subseteq 2^E$ mit den Eigenschaften
	\begin{enumerate}
		\item $\emptyset \in \I$, \label{def:1}
		\item Wenn $I_1 \in \I$ und $I_2 \subseteq I_1$, dann $I_2 \in \I$, \label{def:2}
		\item Wenn $I_1, I_2 \in \I$ mit $|I_1| < |I_2|$, dann existiert ein $e \in I_2$, sodass $I_1 \cup \{e\} \in \I$. \label{def:3}
	\end{enumerate}
\end{definition}
Wir nennen die Mengen in $\I$ \emph{unabhängige Mengen}. Die Eigenschaften \ref{def:1} - \ref{def:3} lassen sich auch lesen als
\begin{enumerate}
	\item Es gibt eine unabhängige Menge,
	\item Unabhängigkeit ist \emph{erblich},
	\item Unabhängige Mengen können solange erweitert werden bis wir keine größere unabhängige Menge mehr kennen.
\end{enumerate}
Eine maximale unabhängige Menge heißt \emph{Basis}. Wegen Eigenschaft~\ref{def:3} sind alle Basen eines Matroids gleich groß.

Wenn einem Matroide (in Reinform) selbst zu abstrakt erscheinen, kann man an konkrete Strukturen denken, die Matroide sind. Zum Beispiel Vektorräume oder kreisfreie Graphen.

Eine \emph{Gewichtsfunktion} auf einem Matroiden $M = (E, \I)$ ist eine Funktion $w \colon E \to \mathbb{R}_+$. Für abkürzende Notation schreiben wir für Teilmengen $A \subseteq E$
$$
	w(A) \coloneqq \sum_{e \in A} w(e).
$$

Angenommen wir haben einen Matroid $M = (E, \I)$ und zugehörige Gewichtsfunktion $w \colon E \to \mathbb{R}_+$. Eine natürliche Frage ist nun: ''Welche unabhängige Teilmenge hat das größte Gewicht? ``
Es ist klar, dass bei nicht-negativen Gewichten immer eine Basis gewichtsmaximal ist. Allerdings muss die Lösung nicht eindeutig sein. Umgekehrt ließe sich auch fragen, welche unabhängige Teilmenge von maximaler Kardinalität das kleinste Gewicht hat.

Ein Greedy-Algorithmus arbeitet informell wie folgt: Starte mit der leeren Menge $\emptyset$ und wähle in jedem Schritt ein Element aus, dass die Menge erweitert, sodass sie unabhängig bleibt, mit maximalem Gewicht. D.h. wir wählen stets das Element aus, welches mit Blick auf die Zielfunktion ''am besten aussieht``.

Sei $E$ eine endliche Menge und $\F$ eine nicht-leere Familie von Teilmengen von $E$, sodass jede Teilmenge einer Menge $F \in \F$ ebenfalls zu $\F$ gehört. Ein Greedy-Algorithmus konstruiert eine endliche Folge $F_0, F_1, \ldots$:
\begin{algorithm}\caption{Greedy-Algorithmus}
	$F_0 \coloneqq \emptyset$.\\
	\For{$i = 0, 1, \ldots$}
		{
		$Z_i \coloneqq \{z \in E \mid z \notin F_i \text{ und } F_i \cup \{z\} \in \F\}$.\\
		\If{$Z_i \neq \emptyset$}
			{
			$z_i \coloneqq \arg\max_{z \in Z_i} w(z)$.\\
			$F_{i+1} \coloneqq F_i \cup \{z_i\}$.
			}
		\Else
			{
			return $F_i$.
			}
		}
\end{algorithm}

\begin{theorem}\label{satz:1}
	Falls $\F$ die Familie unabhängiger Mengen eines Matroids ist, dann konstruiert der Algorithmus eine unabhängige Teilmenge mit maximalem Gewicht.
	\begin{proof}
		Angenommen der Algorithmus wählt die Basis $B$ (der Algorithmus wählt offensichtlich immer eine Basis, da er erst abbricht, wenn keine Elemente mehr hinzugefügt werden können). Sei $T$ eine Basis mit maximalem Gewicht und so, dass $|B \cap T|$ maximal ist. Falls gilt $B = T$, so sind wir bereits fertig. Ansonsten sei $e \in B \setminus T$. Da $T$ eine Basis ist und $e \notin T$ ist $T \cup \{e\}$ abhängig. Wähle $C \subseteq T \cup \{e\}$ als kleinste abhängige Teilmenge. Beachte, dass $e \in C$, sonst wäre $C \subseteq T$ und somit $T$ bereits abhängig, also keine Basis. Sei $f \in C \setminus B$ beliebig. Dann ist $C \setminus \{f\}$ unabhängig und $C \setminus \{f\} \subseteq T \cup \{e\}$. Sei nun $C_0 \coloneqq C \setminus \{f\}$. Für alle $i \in [r]$ mit $r \coloneqq |T|-|C_0|$, verwenden wir \ref{def:3} um ein $z_i \in T \setminus C_i$ zu finden, sodass $C_{i+1} \coloneqq C_i \cup \{z_i\}$ unabhängig ist. Zuletzt setze $T' \coloneqq C_r$. Beachte, dass 
		$$
			C \setminus \{f\} \subseteq T' \subseteq T \cup \{e\}
		$$
		mit $f \notin T'$ und $|T'| = |T|$. Also ist $T' = (T \setminus \{f\}) \cup \{e\}$ eine Basis.
		$T$ ist eine gewichtsmaximale unabhängige Menge, d.h. $w(T') \leq w(T)$ und damit $w(f) \geq w(e)$. andererseits hat aber der Algorithmus $e$ ausgewählt, nicht $f$. Sei $X$ die Menge von Elementen, die vor $e$ ausgewählt wurden. Dann ist $X \subseteq T$ und $X \cup \{f\} \subseteq T$. Also war $f$ zum Zeitpunkt wo $e$ ausgewählt wurde verfügbar für den Algorithmus. Somit gilt $w(e) \geq w(f)$ und damit insgesamt $w(e) = w(f)$ und somit $w(T') = w(T)$. Nun ist $T'$ ebenfalls eine gewichtsmaximale unabhängige Menge mit 
		$$
			|T' \cap B| > |T \cap B|,
		$$
		aber das ist ein Widerspruch zur Wahl von $T$.
	\end{proof}
\end{theorem}

\begin{theorem}
	Sei $\F$ eine erbliche, nicht-leere Familie von Teilmengen von einer Menge $E$. Falls der Algorithmus funktioniert auf $(\F, w)$ wür jede Gewichtsfunktion $w$, dann ist $\F$ die Menge aller unabhängiger Teilmengen eines Matroids auf $E$.
	\begin{proof}
		Die Eigenschaften~\ref{def:1} und~\ref{def:2} sind trivialerweise erfüllt. Für~\ref{def:2} wählen wir $A, B \in \F$ mit $|A| < |B|$. Sei $t \coloneqq |A \cap B|$, $s \coloneqq |A \setminus B| = |A| - t$, $r \coloneqq |B| - |A| = |B| - t - s > 0$. Sei außerdem die Gewichtsfunktion $w \colon E \to \mathbb{R}_+$ gegeben durch
		$$
			w(e) = \begin{cases}
				s+r+2, & e \in A\\
				s+2, & e \in B \setminus A\\
				0, & e \notin A \cup B.
			\end{cases}
		$$
		Wenden wir den Algorithmus an auf $(\F, w)$, dann wählt dieser zunächst alle Elemente von $A$ aus und endet nach Satz~\ref{satz:1} mit einer gewichtsmaximalen unabhängigen Teilmenge aus $\F$. Aber 
		\begin{align*}
			w(B) &= t(s+r+2) + (s+r)(s+2)\\
			&= t(s+r+2) + s(s+r+2) + 2r\\
			&= w(A) + 2r\\
			&> w(A).
		\end{align*}
		Also fügt der Algorithmus nach den Elementen von $A$ noch mindestens ein weiteres Element $e \notin A$ ein mit $w(e) > 0$ und $A \cup \{e\} \in \F$. Nach Definition von $w$ muss $e \in B$ sein und somit ist Eigenschaft~\ref{def:3} erfüllt.
	\end{proof}
\end{theorem}


\end{document}
