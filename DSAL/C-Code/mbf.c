#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

typedef struct Edge
{
	int o;		// origin
	int d;		// destination
	int w;		// weight
} Edge;

typedef struct Graph
{
	int n, m;	// number of vertices and edges
	Edge* edge;	// array of edges
} Graph;

int min(int a, int b) {
	return (a <= b) ? a : b;
}

Graph* construct_graph(int n, int m) {
	Graph* G = (Graph*) malloc(sizeof(Graph));
	G->n = n;
	G->m = m;
	G->edge = (Edge*) malloc(m * sizeof(Edge));
	return G;
}

int BMF(Graph* G, int s) {
	int d[G->n];
	int i, j, u, v, w;

	for (i = 0; i < G->n; i++) {
		d[i] = INT_MAX;
	}
	d[s] = 0;

	for (i = 1; i <= G->n-1; i++) {
		for (j = 0; j < G->m; j++) {
			u = G->edge[j].o;
			v = G->edge[j].d;
			w = G->edge[j].w;

			d[v] = min(d[v], d[u] + w);
		}
	}

	for (i = 0; i < G->m; i++) {
		u = G->edge[i].o;
		v = G->edge[i].d;
		w = G->edge[i].w;

		if (d[v] > d[u] + w) {
			return 0;
		}
	}
	return 1;
}

int main(int argc, char** argv) {
	Graph* G;
	int s;
	
	if (BMF(G, s)) {
		printf("There exists a negative cycle.\n");
	} else {
		printf("There is no negative cycle.\n");
	}

	return 0;
}
